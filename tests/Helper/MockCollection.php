<?php

namespace ServiceCore\Contact\Test\Helper;

use Closure;
use Doctrine\Common\Collections\Collection;

class MockCollection implements Collection
{
    public function add($element)
    {
        // TODO: Implement add() method.
    }

    public function clear()
    {
        // TODO: Implement clear() method.
    }

    public function contains($element)
    {
        // TODO: Implement contains() method.
    }

    public function isEmpty()
    {
        // TODO: Implement isEmpty() method.
    }

    public function remove($key)
    {
        // TODO: Implement remove() method.
    }

    public function removeElement($element)
    {
        // TODO: Implement removeElement() method.
    }

    public function containsKey($key)
    {
        // TODO: Implement containsKey() method.
    }

    public function get($key)
    {
        // TODO: Implement get() method.
    }

    public function getKeys()
    {
        // TODO: Implement getKeys() method.
    }

    public function getValues()
    {
        // TODO: Implement getValues() method.
    }

    public function set($key, $value)
    {
        // TODO: Implement set() method.
    }

    public function toArray()
    {
        // TODO: Implement toArray() method.
    }

    public function first()
    {
        // TODO: Implement first() method.
    }

    public function last()
    {
        // TODO: Implement last() method.
    }

    public function key()
    {
        // TODO: Implement key() method.
    }

    public function current()
    {
        // TODO: Implement current() method.
    }

    public function next()
    {
        // TODO: Implement next() method.
    }

    public function exists(Closure $p)
    {
        // TODO: Implement exists() method.
    }

    public function filter(Closure $p)
    {
        // TODO: Implement filter() method.
    }

    public function forAll(Closure $p)
    {
        // TODO: Implement forAll() method.
    }

    public function map(Closure $func)
    {
        // TODO: Implement map() method.
    }

    public function partition(Closure $p)
    {
        // TODO: Implement partition() method.
    }

    public function indexOf($element)
    {
        // TODO: Implement indexOf() method.
    }

    public function slice($offset, $length = null)
    {
        // TODO: Implement slice() method.
    }

    public function getIterator()
    {
        // TODO: Implement getIterator() method.
    }

    public function offsetExists($offset)
    {
        // TODO: Implement offsetExists() method.
    }

    public function offsetGet($offset)
    {
        // TODO: Implement offsetGet() method.
    }

    public function offsetSet($offset, $value)
    {
        // TODO: Implement offsetSet() method.
    }

    public function offsetUnset($offset)
    {
        // TODO: Implement offsetUnset() method.
    }

    public function count()
    {
        // TODO: Implement count() method.
    }
}
