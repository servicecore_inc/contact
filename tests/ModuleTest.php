<?php

namespace ServiceCore\Contact\Test;

use PHPUnit\Framework\TestCase;
use ServiceCore\Contact\Module;
use ServiceCore\Contact\Strategy\Contact as ContactStrategy;
use ServiceCore\Contact\Strategy\Contacts as ContactsStrategy;
use ServiceCore\Contact\Validator\Contacts as ContactsValidator;

class ModuleTest extends TestCase
{
    public function testGetConfig(): void
    {
        $module = new Module();
        $config = $module->getConfig();

        $this->assertArrayHasKey('service_manager', $config);
        $this->assertIsArray($config['service_manager']);
        $this->assertArrayHasKey('factories', $config['service_manager']);
        $this->assertIsArray($config['service_manager']['factories']);
        $this->assertArrayHasKey(ContactStrategy::class, $config['service_manager']['factories']);
        $this->assertArrayHasKey(ContactsStrategy::class, $config['service_manager']['factories']);

        $this->assertArrayHasKey('validators', $config);
        $this->assertIsArray($config['validators']);
        $this->assertArrayHasKey('factories', $config['validators']);
        $this->assertIsArray($config['validators']['factories']);
        $this->assertArrayHasKey(ContactsValidator::class, $config['validators']['factories']);
    }
}
