<?php

namespace ServiceCore\Contact\Test\Validator;

use PHPUnit\Framework\TestCase;
use ServiceCore\Contact\Validator\Contacts;
use stdClass;

class ContactsTest extends TestCase
{
    public function testIsValidReturnsFalseIfItemNotArray(): void
    {
        $validator = new Contacts();
        $values    = 'foo';

        $this->assertFalse($validator->isValid($values));
        $this->assertArrayHasKey(Contacts::ITEM_NOT_ARRAY, $validator->getMessages());
    }

    public function testIsValidReturnsFalseIfSubItemNotArray(): void
    {
        $validator = new Contacts();
        $values    = ['fooBar'];

        $this->assertFalse($validator->isValid($values));
        $this->assertArrayHasKey(Contacts::ITEM_NOT_ARRAY, $validator->getMessages());
    }

    public function testIsValidReturnsFalseIfRequiredFieldsMissing(): void
    {
        $validator = new Contacts();
        $values    = [['foo' => 'bar']];

        $this->assertFalse($validator->isValid($values));
        $this->assertArrayHasKey(Contacts::REQUIRED_FIELDS_MISSING, $validator->getMessages());
    }

    public function testIsValidReturnsFalseIfPhoneInvalid(): void
    {
        $validator      = new Contacts();
        $requiredFields = Contacts::$requiredFields;
        $fields         = [];

        foreach ($requiredFields as $requiredField) {
            $fields[$requiredField] = 'foo';
        }

        $fields['phone'] = 'fooBar';
        $values          = [$fields];

        $this->assertFalse($validator->isValid($values));
        $this->assertArrayHasKey(Contacts::PHONE_NUMBER_INVALID, $validator->getMessages());
    }

    public function testIsValidReturnsFalseIfEmailInvalid(): void
    {
        $validator      = new Contacts();
        $requiredFields = Contacts::$requiredFields;
        $fields         = [];

        foreach ($requiredFields as $requiredField) {
            $fields[$requiredField] = 'foo';
        }

        $fields['email'] = 'fooBar';
        $values          = [$fields];

        $this->assertFalse($validator->isValid($values));
        $this->assertArrayHasKey(Contacts::EMAIL_INVALID, $validator->getMessages());
    }

    public function testIsValidReturnsTrue(): void
    {
        $validator      = new Contacts();
        $requiredFields = Contacts::$requiredFields;
        $fields         = [];

        foreach ($requiredFields as $requiredField) {
            $fields[$requiredField] = 'foo';
        }

        $values = [$fields];

        $this->assertTrue($validator->isValid($values, new stdClass()));
    }
}
