<?php

namespace ServiceCore\Contact\Data;

use Doctrine\ORM\Mapping as ORM;
use ServiceCore\Contact\RoleData\AbstractContactInterface;

abstract class AbstractContact implements AbstractContactInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", length=10, nullable=false, options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Id()
     */
    protected $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=127, nullable=true)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=127, nullable=false)
     */
    protected $firstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="last_name", type="string", length=127, nullable=true)
     */
    protected $lastName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=63, nullable=true)
     */
    protected $phone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone_ext", type="string", length=63, nullable=true)
     */
    protected $phoneExt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string|null $title
     *
     * @return AbstractContact
     */
    public function setTitle(?string $title = null): AbstractContact
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $firstName
     *
     * @return AbstractContact
     */
    public function setFirstName(string $firstName): AbstractContact
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $lastName
     *
     * @return AbstractContact
     */
    public function setLastName(?string $lastName = null): AbstractContact
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     *
     * @return AbstractContact
     */
    public function setEmail(?string $email = null): AbstractContact
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     *
     * @return AbstractContact
     */
    public function setPhone(?string $phone = null): AbstractContact
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhoneExt(): ?string
    {
        return $this->phoneExt;
    }

    /**
     * @param string|null $phoneExt
     *
     * @return AbstractContact
     */
    public function setPhoneExt(?string $phoneExt): AbstractContact
    {
        $this->phoneExt = $phoneExt;

        return $this;
    }
}
