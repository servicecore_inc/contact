<?php

namespace ServiceCore\Contact\Validator;

use Laminas\Validator\AbstractValidator;
use Laminas\Validator\EmailAddress as EmailValidator;
use ServiceCore\Validate\Validator\Phone as PhoneValidator;

class Contacts extends AbstractValidator
{
    // Validation keys
    public const ITEM_NOT_ARRAY          = 'itemNotArray';
    public const REQUIRED_FIELDS_MISSING = 'requiredFieldsMissing';
    public const PHONE_NUMBER_INVALID    = 'phoneNumberInvalid';
    public const EMAIL_INVALID           = 'emailInvalid';

    // Validation verbiage
    public const ITEM_NOT_ARRAY_VERBIAGE          = 'Field is not an array';
    public const REQUIRED_FIELDS_MISSING_VERBIAGE = 'Missing `firstName`';
    public const PHONE_NUMBER_INVALID_VERBIAGE    = 'Phone number is not valid.';
    public const EMAIL_INVALID_VERBIAGE           = 'Email is not valid.';

    /** @var array */
    public static $requiredFields = [
        'firstName',
    ];

    /** @var array */
    protected $messageTemplates = [
        self::ITEM_NOT_ARRAY          => self::ITEM_NOT_ARRAY_VERBIAGE,
        self::REQUIRED_FIELDS_MISSING => self::REQUIRED_FIELDS_MISSING_VERBIAGE,
        self::PHONE_NUMBER_INVALID    => self::PHONE_NUMBER_INVALID_VERBIAGE,
        self::EMAIL_INVALID           => self::EMAIL_INVALID_VERBIAGE,
    ];

    public function isValid($values, $context = null): bool
    {
        if (!\is_array($values)) {
            $this->error(self::ITEM_NOT_ARRAY);

            return false;
        }

        if (\count(\array_filter($values)) !== 0) {
            foreach ($values as $contact) {
                if (!\is_array($contact)) {
                    $this->error(self::ITEM_NOT_ARRAY);

                    return false;
                }

                if (!$this->hasRequiredFields($contact)) {
                    return false;
                }

                if (!$this->hasValidFields($contact)) {
                    return false;
                }
            }
        }

        return true;
    }

    private function hasValidFields(array $data): bool
    {
        $phoneValidator = new PhoneValidator();
        $emailValidator = new EmailValidator();

        if (\array_key_exists('phone', $data) && $data['phone'] !== null && !$phoneValidator->isValid($data['phone'])) {
            $this->error(self::PHONE_NUMBER_INVALID);

            return false;
        }

        if (\array_key_exists('email', $data) && $data['email'] !== null && !$emailValidator->isValid($data['email'])) {
            $this->error(self::EMAIL_INVALID);

            return false;
        }

        return true;
    }

    private function hasRequiredFields(array $contact): bool
    {
        if (\count(\array_intersect_key(\array_flip(self::$requiredFields), $contact))
            !== \count(self::$requiredFields)
        ) {
            $this->error(self::REQUIRED_FIELDS_MISSING);

            return false;
        }

        return true;
    }
}
