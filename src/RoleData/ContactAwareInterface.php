<?php

namespace ServiceCore\Contact\RoleData;

use Doctrine\Common\Collections\Collection;
use ServiceCore\Contact\Data\AbstractContact;

interface ContactAwareInterface
{
    public function getContacts(): Collection;

    public function setContacts(Collection $contacts);

    public function addContact(AbstractContact $contact);

    public function addContacts(Collection $contacts);

    public function removeContacts(Collection $contacts);
}
