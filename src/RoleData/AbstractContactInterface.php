<?php

namespace ServiceCore\Contact\RoleData;

interface AbstractContactInterface
{
    public function getId(): int;

    public function setTitle(?string $title = null);

    public function getTitle(): ?string;

    public function setFirstName(string $firstName);

    public function getFirstName(): string;

    public function setLastName(?string $lastName = null);

    public function getLastName(): ?string;

    public function getEmail(): ?string;

    public function setEmail(?string $email = null);

    public function getPhone(): ?string;

    public function setPhone(?string $phone = null);
}
