<?php

namespace ServiceCore\Contact\Helper;

use Doctrine\Common\Collections\Collection;
use ServiceCore\Contact\Data\AbstractContact;

trait ContactAware
{
    /** @var  Collection */
    private $contacts;

    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function setContacts(Collection $contacts): self
    {
        $this->contacts = $contacts;

        return $this;
    }

    public function removeContacts(Collection $contacts): self
    {
        foreach ($contacts as $contact) {
            if ($this->contacts->contains($contact)) {
                $this->contacts->removeElement($contact);
            }
        }

        return $this;
    }

    public function addContact(AbstractContact $contact): self
    {
        $this->contacts->add($contact);

        return $this;
    }

    public function addContacts(Collection $contacts): self
    {
        foreach ($contacts as $contact) {
            if (!$this->contacts->contains($contact)) {
                $this->contacts->add($contact);
            }
        }

        return $this;
    }
}
