<?php

namespace ServiceCore\Contact\Strategy;

use Doctrine\Common\Collections\Collection;
use DoctrineModule\Stdlib\Hydrator\Strategy\AbstractCollectionStrategy;
use ServiceCore\Contact\Data\AbstractContact;
use ServiceCore\Contact\Strategy\Contact as ContactStrategy;

class Contacts extends AbstractCollectionStrategy
{
    /**
     * @param Collection|array $values
     *
     * @return array
     */
    public function extract($values): array
    {
        $strategy = new ContactStrategy();

        return $values->map(static function (AbstractContact $contact) use ($strategy) {
            return $strategy->extract($contact);
        })->toArray();
    }

    /**
     * @param mixed $values
     *
     * @return mixed
     */
    public function hydrate($values)
    {
        return $values;
    }
}
