<?php

namespace ServiceCore\Contact\Strategy;

use Laminas\Hydrator\Strategy\DefaultStrategy;
use ServiceCore\Contact\Data\AbstractContact as AbstractContactEntity;

class Contact extends DefaultStrategy
{
    public function extract($value): ?array
    {
        $contact = null;

        if ($value instanceof AbstractContactEntity) {
            $contact = [
                'id'        => $value->getId(),
                'firstName' => $value->getFirstName(),
                'lastName'  => $value->getLastName(),
                'title'     => $value->getTitle(),
                'phone'     => $value->getPhone(),
                'phoneExt'  => $value->getPhoneExt(),
                'email'     => $value->getEmail()
            ];
        }

        return $contact;
    }
}
