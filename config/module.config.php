<?php

use Laminas\ServiceManager\Factory\InvokableFactory;
use ServiceCore\Contact\Strategy\Contact as ContactStrategy;
use ServiceCore\Contact\Strategy\Contacts as ContactsStrategy;
use ServiceCore\Contact\Validator\Contacts as CustomerContactsValidator;

return [
    'service_manager' => [
        'factories' => [
            ContactStrategy::class  => InvokableFactory::class,
            ContactsStrategy::class => InvokableFactory::class
        ]
    ],
    'validators'      => [
        'factories'  => [
            CustomerContactsValidator::class => InvokableFactory::class,
        ]
    ],
];
